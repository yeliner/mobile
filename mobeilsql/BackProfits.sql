USE [mobile]
GO

/****** Object:  Table [dbo].[BackProfits]    Script Date: 03/28/2017 14:28:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[BackProfits](
	[BackProfitsId] [int] IDENTITY(1,1) NOT NULL,
	[BackProfitsNum] [varchar](20) NOT NULL,
	[BackProfitsName] [varchar](20) NOT NULL,
	[BackProfitsRemark] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BackProfitsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


