USE [mobile]
GO

/****** Object:  Table [dbo].[PhoneType]    Script Date: 03/28/2017 14:34:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[PhoneType](
	[PhoneTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PhoneTypeNum] [varchar](20) NOT NULL,
	[PhoneTypeName] [varchar](20) NOT NULL,
	[PhoneModel] [varchar](20) NOT NULL,
	[TakeOutPrice] [float] NOT NULL,
	[BasicBack] [float] NOT NULL,
	[AmountBack] [float] NOT NULL,
	[PhoneTypeRemark] [varchar](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[PhoneTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


