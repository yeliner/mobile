USE [mobile]
GO

/****** Object:  Table [dbo].[SupplierSort]    Script Date: 03/28/2017 14:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SupplierSort](
	[SupplierSortId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierSortNum] [varchar](20) NOT NULL,
	[SupplierSortName] [varchar](20) NOT NULL,
	[SupplierSortRemark] [varchar](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[SupplierSortId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


