USE [mobile]
GO

/****** Object:  Table [dbo].[CashBack]    Script Date: 03/28/2017 14:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[CashBack](
	[CashBackId] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [varchar](20) NOT NULL,
	[FinishDate] [varchar](20) NOT NULL,
	[CashAmount] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[CashBackNum] [float] NOT NULL,
	[CashBackRemark] [varchar](30) NOT NULL,
	[SupplierId] [int] NOT NULL,
	[status] [bit] NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[CashBack] ADD [CheckDate] [varchar](20) NULL
ALTER TABLE [dbo].[CashBack] ADD [InputDate] [varchar](20) NULL
/****** Object:  Index [PK__CashBack__86B963C4173876EA]    Script Date: 03/28/2017 14:36:17 ******/
ALTER TABLE [dbo].[CashBack] ADD PRIMARY KEY CLUSTERED 
(
	[CashBackId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CashBack]  WITH CHECK ADD  CONSTRAINT [FK_CashBack_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO

ALTER TABLE [dbo].[CashBack] CHECK CONSTRAINT [FK_CashBack_Supplier]
GO

ALTER TABLE [dbo].[CashBack] ADD  CONSTRAINT [DF_CashBack_status]  DEFAULT ((0)) FOR [status]
GO


