USE [mobile]
GO

/****** Object:  Table [dbo].[payment]    Script Date: 03/28/2017 14:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[payment](
	[CycleCashId] [int] IDENTITY(1,1) NOT NULL,
	[BeginDate] [varchar](20) NULL,
	[EndDate] [varchar](20) NULL,
	[CycleCash] [float] NULL,
	[SupplierId] [int] NULL,
	[CycleStatus] [bit] NULL,
 CONSTRAINT [PK_CycleCash] PRIMARY KEY CLUSTERED 
(
	[CycleCashId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[payment]  WITH CHECK ADD  CONSTRAINT [FK_CycleCash_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO

ALTER TABLE [dbo].[payment] CHECK CONSTRAINT [FK_CycleCash_Supplier]
GO


