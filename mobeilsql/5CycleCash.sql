USE [mobile]
GO

/****** Object:  Table [dbo].[CycleCash]    Script Date: 03/28/2017 14:36:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CycleCash](
	[cycleId] [int] IDENTITY(1,1) NOT NULL,
	[supplierId] [int] NULL,
	[incomeCash] [float] NULL,
	[incomeTime] [nvarchar](50) NULL,
 CONSTRAINT [PK_CycleCash_1] PRIMARY KEY CLUSTERED 
(
	[cycleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CycleCash]  WITH CHECK ADD  CONSTRAINT [FK_CycleCash_Supplier1] FOREIGN KEY([supplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO

ALTER TABLE [dbo].[CycleCash] CHECK CONSTRAINT [FK_CycleCash_Supplier1]
GO


