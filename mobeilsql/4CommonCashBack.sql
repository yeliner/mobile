USE [mobile]
GO

/****** Object:  Table [dbo].[CommonCashBack]    Script Date: 03/28/2017 14:35:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommonCashBack](
	[CommonCashBackId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierId] [int] NOT NULL,
	[PhoneTypeId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[UnitPrice] [float] NOT NULL,
	[CashBackNum] [float] NOT NULL,
 CONSTRAINT [PK__CommonCa__F67620AD1BFD2C07] PRIMARY KEY CLUSTERED 
(
	[CommonCashBackId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CommonCashBack]  WITH CHECK ADD  CONSTRAINT [FK__CommonCas__Phone__1FCDBCEB] FOREIGN KEY([PhoneTypeId])
REFERENCES [dbo].[PhoneType] ([PhoneTypeId])
GO

ALTER TABLE [dbo].[CommonCashBack] CHECK CONSTRAINT [FK__CommonCas__Phone__1FCDBCEB]
GO

ALTER TABLE [dbo].[CommonCashBack]  WITH CHECK ADD  CONSTRAINT [FK__CommonCas__Suppl__1DE57479] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO

ALTER TABLE [dbo].[CommonCashBack] CHECK CONSTRAINT [FK__CommonCas__Suppl__1DE57479]
GO


