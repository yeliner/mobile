USE [mobile]
GO

/****** Object:  Table [dbo].[Organization]    Script Date: 03/28/2017 14:32:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[Organization](
	[OrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationNum] [varchar](20) NOT NULL,
	[OrganizationName] [varchar](20) NOT NULL,
	[OrganizationPhone] [varchar](20) NOT NULL,
	[OrganizationAddress] [varchar](50) NOT NULL,
	[OrganizationCharge] [varchar](50) NOT NULL,
	[OrganizationRemark] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


