USE [mobile]
GO

/****** Object:  Table [dbo].[Supplier]    Script Date: 03/28/2017 14:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Supplier](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierNum] [varchar](20) NOT NULL,
	[SupplierName] [varchar](20) NOT NULL,
	[SupplierSortId] [int] NOT NULL,
	[SupplierCharge] [varchar](20) NOT NULL,
	[SupplierPhone] [varchar](20) NOT NULL,
	[SupplierFax] [varchar](20) NOT NULL,
	[SupplierAddress] [varchar](50) NOT NULL,
	[SupplierRemark] [varchar](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD FOREIGN KEY([SupplierSortId])
REFERENCES [dbo].[SupplierSort] ([SupplierSortId])
GO


