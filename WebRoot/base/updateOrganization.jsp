<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@include file="/share/taglib.jsp" %>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
-->
</style>
<link href="<%=path %>/images/skin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
function add(){
	form1.submit();
}
//-->
</script>
<body>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="17" height="29" valign="top"
				background="<%=path %>/images/mail_leftbg.gif">
				<img src="<%=path %>/images/left-top-right.gif" width="17" height="29" />
			</td>
			<td width="935" height="29" valign="top"
				background="<%=path %>/images/content-bg.gif">
				<table width="100%" height="31" border="0" cellpadding="0"
					cellspacing="0" class="left_topbg" id="table2">
					<tr>
						<td height="31">
							<div class="titlebt">
								基本设置
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="16" valign="top" background="<%=path %>/images/mail_rightbg.gif">
				<img src="<%=path %>/images/nav-right-bg.gif" width="16" height="29" />
			</td>
		</tr>
		<tr>
			<td height="71" valign="middle" background="<%=path %>/images/mail_leftbg.gif">
				&nbsp;
			</td>
			<td valign="top" bgcolor="#F7F8F9">
				<table width="100%" height="138" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td height="13" valign="top">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td valign="top">
							<table width="98%" border="0" align="center" cellpadding="0"
								cellspacing="0">
								<tr>
									<td class="left_txt">
										基础设置管理-&gt;组织机构设置管理-&gt;修改组织机构 
									</td>
								</tr>
								<tr>
									<td height="20">
										<table width="100%" height="1" border="0" cellpadding="0"
											cellspacing="0" bgcolor="#CCCCCC">
											<tr>
												<td></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" height="31" border="0" cellpadding="0"
											cellspacing="0" class="nowtable">
											<tr>
												<td class="left_bt2">
													&nbsp;&nbsp;&nbsp;&nbsp;修改组织机构
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<form name="form1" method="post" action="organization!update.action">
											<input name="organizationId" type="hidden" id="title" size="30" value="${organization.organizationId }"/>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													组织机构名称：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="organizationName" type="text" id="title" size="30" value="${organization.organizationName }"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													组织机构联系人：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="organizationCharge" type="text" id="title" size="30" value="${organization.organizationCharge }"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													组织机构电话：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="organizationPhone" type="text" id="title" size="30" value="${organization.organizationPhone }"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													组织机构地址：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="organizationAddress" type="text" id="title" size="30" value="${organization.organizationAddress }"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													组织机构备注：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="organizationRemark" type="text" id="title" size="30" value="${organization.organizationRemark }"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
														<img src="<%=path %>/images/back.jpg" onclick="window.location.href='organization!show.action'" style="cursor: pointer;"/>
												</td>
												<td bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td height="30" bgcolor="#f2f2f2">
													<img src="<%=path %>/images/save.jpg" onclick="add();" style="cursor: pointer;"/>
												</td>
												<td height="30" bgcolor="#f2f2f2" class="left_txt">
													
												</td>
											</tr>
											
	</table>
</body>
