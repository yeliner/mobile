<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ page import="entity.*" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@include file="/share/taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
-->
</style>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function show(page){
  window.location.href='organization!showOrganization.action?page='+page;
}
</script>
  </head>
 <%--  <%List<Organization> ss=(ArrayList<Organization>)request.getSession().getAttribute("OrganizationList"); %> --%>
  
  <body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="17" height="29" valign="top"
				background="images/mail_leftbg.gif">
				<img src="images/left-top-right.gif" width="17" height="29" />
			</td>
			<td width="935" height="29" valign="top"
				background="images/content-bg.gif">
				<table width="100%" height="31" border="0" cellpadding="0"
					cellspacing="0" class="left_topbg" id="table2">
					<tr>
						<td height="31">
							<div class="titlebt">
								基础设置
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="16" valign="top" background="images/mail_rightbg.gif">
				<img src="images/nav-right-bg.gif" width="16" height="29" />
			</td>
		</tr>
		<tr>
			<td height="71" valign="middle" background="images/mail_leftbg.gif">
				&nbsp;
			</td>
			<td valign="top" bgcolor="#F7F8F9">
				<table width="100%" height="138" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td height="13" valign="top">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td valign="top">
							<table width="98%" border="0" align="center" cellpadding="0"
								cellspacing="0">
								<tr>
									<td class="left_txt">
										当前位置：基础设置管理-&gt;组织机构设置管理
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" height="31" border="0" cellpadding="0"
											cellspacing="0" class="nowtable">
												
											<tr>
												<td class="left_bt2">
													&nbsp;&nbsp;&nbsp;&nbsp;组织机构显示
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" height="31" border="0" cellpadding="0"
											cellspacing="0" class="nowtable">
												
											<tr>
												<td style="font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 25px;
	font-weight: bold;
	color: #333333;
	text-align:right;
	">
													<img src="images/add.jpg" onclick="window.location.href='base/addOrganization.jsp'" style="cursor: pointer;"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<form name="form1" method="POST" action="">
											<tr >
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2" style= "top-padding:10px; " align= "center ">
													编号
												</td>
											<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													名称
													</td>
													<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													负责人
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													电话
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													地址
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													备注
												</td>
													<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													操作
												</td>
											</tr>
										<%-- 	<s:iterator value="#session.OrganizationList" var="s" status="sta">	
												<tr>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${s.organizationNum }
												</td>
											<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${s.organizationName }
												</td>
													<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${s.organizationCharge }
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${s.organizationPhone }
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${s.organizationAddress }
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${s.organizationRemark }
												</td>
													<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													<a href="organization!goUpdate.action?organizationId=${s.organizationId}">修改</a>
													<a href="organization!delete.action?organizationId=${s.organizationId}">删除</a>
												</td>
											</tr>
											</s:iterator>	 --%>
											<c:forEach var="item" items="${pageUtil.currentList}">
													<tr>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.organizationNum }
												</td>
											<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.organizationName }
												</td>
													<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.organizationCharge }
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.organizationPhone }
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.organizationAddress }
												</td>
												<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.organizationRemark }
												</td>
													<td width="10%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													<a href="organization!goUpdate.action?organizationId=${item.organizationId}">修改</a>
													<a href="organization!delete.action?organizationId=${item.organizationId}">删除</a>
												</td>
											</tr>
											</c:forEach> 

											<tr>
												<td height="17" colspan="4" align="right">
													&nbsp;
												</td>
											</tr>
										
							<tr style="text-align:right;" align="right">
												<td height="30" colspan="9" align="right" style="font-family: Arial, Helvetica, sans-serif;
						font-size: 12px;
						line-height: 25px;
						color: #000000;
						text-align:right;">
													
												共有<strong>${pageUtil.allRecord }</strong> 条记录，当前第<strong> ${pageUtil.currentPage }</strong> 页，共 <strong>${pageUtil.allPage }</strong> 页
												<img src="images/main_54.gif" width="40" height="15" onclick="show(1)" style="cursor: pointer;"/>
          <img src="images/main_56.gif" width="45" height="15" onclick="show(${pageUtil.currentPage-1})" style="cursor: pointer;"/>
           <img src="images/main_58.gif" width="45" height="15" onclick="show(${pageUtil.currentPage+1})" style="cursor: pointer;"/>
           <img src="images/main_60.gif" width="40" height="15" onclick="show(${pageUtil.allPage})" style="cursor: pointer;"/>
           转到
           
              <input type="text" name="page" id="textfield"  style="width:20px; height:18px; font-size:12px; border:solid 1px #7aaebd;"/>
            
           页
              <img src="images/main_62.gif" width="26" height="15"  style="cursor: pointer;"/>
        <!--    <img src="images/main_62.gif" width="26" height="15" onclick="show(page.value)" style="cursor: pointer;"/> -->
												
												</td>
											</tr>
	</table>
  </body>
</html>
