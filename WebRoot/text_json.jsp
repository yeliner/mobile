<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<link href="common/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="common/common.js"></script>
<script type="text/javascript" src="common/popcalendar.js"></script>
</head>

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
 <script type="text/javascript">
 
 function getUserByName(){
 var userName=$("#userName").val();
 if(userName!=null){
  $.ajax({
    type:"post",
    url:"simpljson/jsonAction!getUserByName.action",
    dataType:"json",
    data:{'userName':userName},
    success:function(returnData){
     	var html="<table><tr><td>编号</td><td>用戶名</td><td>密碼</td>";
     	for(var i=0;i<returnData.length;i++){
     	html+="<tr><td>"+returnData[i].userInfoId+"</td><td>"+returnData[i].userInfoName+"</td><td>"+returnData[i].userInfoPsw+"</td></tr>";
     	}
     	html+="</table>";
     $("#result").html(html);
    }
   });
  }
 else{
 alert("请输入要查找的Name");
 }
   }
  
 function getAll(){
  $.ajax({
    type:"post",
    url:"jsonAction!getAll.action",
    dataType:"json",
    success:function(returnData){
     	var html="<table><tr><td>编号</td><td>用戶名</td><td>密碼</td>";
     	for(var i=0;i<returnData.length;i++){
     	html+="<tr><td>"+returnData[i].userInfoId+"</td><td>"+returnData[i].userInfoName+"</td><td>"+returnData[i].userInfoPsw+"</td></tr>";
     	}
     	html+="</table>";
     $("#result").html(html);
    }
   });
 
 }
  
</script>
<body onload="getAll();">

 <input type="text" id="userName">
  <input type="button" value="根据name获取" id="getByName" onclick="getUserByName();">
 <input type="button" value="获取全部" id="getAll" onclick="getAll();">
  <a href="login.jsp">去登录</a>
 <div  id="result"></div>
</body>
</html>
