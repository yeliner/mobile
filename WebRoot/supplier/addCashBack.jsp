<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@include file="/share/taglib.jsp" %>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
-->
.w{

width: 53%
}
</style>
<link href="<%=path %>/images/skin.css" rel="stylesheet" type="text/css" />
<link href="common/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="common/common.js"></script>
<script type="text/javascript" src="common/popcalendar.js"></script>
<script type="text/javascript">
<!--
function count(method){
	//window.location.href = 'cashBack!countAll.action';
	    //配置动态方法调用
       	  form1.action="cashBack!"+method+".action";
       	  form1.submit();
	
}
function back(){
   form1.cachBack.value=form1.totalCash.value * form1.percentage.value;
}
function add(method){
//配置动态方法调用
       	  form1.action="cashBack!"+method+".action";
       	  form1.submit();
	
}
//-->
</script>
<body>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="17" height="29" valign="top"
				background="<%=path %>/images/mail_leftbg.gif">
				<img src="<%=path %>/images/left-top-right.gif" width="17" height="29" />
			</td>
			<td width="935" height="29" valign="top"
				background="<%=path %>/images/content-bg.gif">
				<table width="100%" height="31" border="0" cellpadding="0"
					cellspacing="0" class="left_topbg" id="table2">
					<tr>
						<td height="31">
							<div class="titlebt">
								供应商业务
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="16" valign="top" background="<%=path %>/images/mail_rightbg.gif">
				<img src="<%=path %>/images/nav-right-bg.gif" width="16" height="29" />
			</td>
		</tr>
		<tr>
			<td height="71" valign="middle" background="<%=path %>/images/mail_leftbg.gif">
				&nbsp;
			</td>
			<td valign="top" bgcolor="#F7F8F9">
				<table width="100%" height="238" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td height="13" valign="top">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td valign="top">
							<table width="98%" border="0" align="center" cellpadding="0"
								cellspacing="0">
								<tr>
									<td class="left_txt">
										供应商业务管理设置-&gt;现金返利-&gt;添加现金返利 
									</td>
								</tr>
								<tr>
									<td height="20">
										<table width="100%" height="1" border="0" cellpadding="0"
											cellspacing="0" bgcolor="#CCCCCC">
											<tr>
												<td></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" height="31" border="0" cellpadding="0"
											cellspacing="0" class="nowtable">
											<tr>
												<td class="left_bt2">
													&nbsp;&nbsp;&nbsp;&nbsp;添加现金返利
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" height="238">
											<form name="form1" method="post" action="">
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													供应商：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<%-- <input name="supplierName" type="text" id="title" size="30" />--%>
													<select name="supplierName" id="title" style="width: 53%">
													<c:forEach var="item" items="${supplierList}">
													<option value="${item.supplierId }">${item.supplierName }</option>
													</c:forEach>
													</select>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													开始日期：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<INPUT TYPE="text" width="38%" height="30"  id="date" name="startTime" value="${cashBackForm.startTime }"> <img src="images/calendar.gif" border="0" onclick="popUpCalendar(this, document.getElementById('date'), 'yyyy-mm-dd',-1,-1,true)">
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													结束日期：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<INPUT TYPE="text" width="38%" height="30"  id="date1" name="endTime" value="${cashBackForm.endTime }"> <img src="images/calendar.gif" border="0" onclick="popUpCalendar(this, document.getElementById('date1'), 'yyyy-mm-dd',-1,-1,true)">
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													现金付款金额：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="12%" height="30" bgcolor="#f2f2f2">
													<input name="totalCash" type="text" class="w" size="30" value="${allCash }"/>
													<img src="<%=path %>/images/save1.jpg" onclick="count('countAll');" style="cursor: pointer;"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													比率：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="percentage" type="text" class="w" size="30" />
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													返利金额：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="cachBack" type="text" class="w" size="30" value=""/>
													<img src="<%=path %>/images/save1.jpg" onclick="back();" style="cursor: pointer;"/>
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													录入时间：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<INPUT TYPE="text" width="38%" height="30"  id="date2" name="inputTime"> <img src="images/calendar.gif" border="0" onclick="popUpCalendar(this, document.getElementById('date2'), 'yyyy-mm-dd',-1,-1,true)">
												</td>
												<td width="45%" height="30" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td width="20%" height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													备注：
												</td>
												<td width="3%" bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td width="32%" height="30" bgcolor="#f2f2f2">
													<input name="cashBackRemark"  class="w" size="30" />
												</td>
												<td width="40%" height="10" bgcolor="#f2f2f2"
													class="left_txt">
													
												</td>
											</tr>
											<tr>
												<td height="30" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
														<img src="<%=path %>/images/back.jpg" onclick="window.location.href='cashBack!show.action'" style="cursor: pointer;"/>
												</td>
												<td bgcolor="#f2f2f2">
													&nbsp;
												</td>
												<td height="30" bgcolor="#f2f2f2">
													<img src="<%=path %>/images/save.jpg" onclick="add('addCashBack');" style="cursor: pointer;"/>
												</td>
												<td height="30" bgcolor="#f2f2f2" class="left_txt">
													
												</td>
											</tr>
											
	</table>
</body>
