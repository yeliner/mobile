<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@include file="/share/taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
-->
</style>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function show(page){
  window.location.href='commonBack!show.action?page='+page;
}
</script>
  </head>
  
  <body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="17" height="29" valign="top"
				background="images/mail_leftbg.gif">
				<img src="images/left-top-right.gif" width="17" height="29" />
			</td>
			<td width="935" height="29" valign="top"
				background="images/content-bg.gif">
				<table width="100%" height="31" border="0" cellpadding="0"
					cellspacing="0" class="left_topbg" id="table2">
					<tr>
						<td height="31">
							<div class="titlebt">
								供应商业务
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="16" valign="top" background="images/mail_rightbg.gif">
				<img src="images/nav-right-bg.gif" width="16" height="29" />
			</td>
		</tr>
		<tr>
			<td height="71" valign="middle" background="images/mail_leftbg.gif">
				&nbsp;
			</td>
			<td valign="top" bgcolor="#F7F8F9">
				<table width="100%" height="138" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td height="13" valign="top">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td valign="top">
							<table width="98%" border="0" align="center" cellpadding="0"
								cellspacing="0">
								<tr>
									<td class="left_txt">
										当前位置：供应商业务设置管理-&gt;一般返利
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" height="31" border="0" cellpadding="0"
											cellspacing="0" class="nowtable">
												
											<tr>
												<td class="left_bt2">
													&nbsp;&nbsp;&nbsp;&nbsp;一般返利显示
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" height="31" border="0" cellpadding="0"
											cellspacing="0" class="nowtable">
												
											<tr>
												<td style="font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 25px;
	font-weight: bold;
	color: #333333;
	text-align:right;
	">
													<img src="images/add.jpg" onclick="window.location.href='commonBack!goAddCommonBack.action'" style="cursor: pointer;"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<form name="form1" method="POST" action="">
											<tr >
												<td width="15%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2" style= "top-padding:10px; ">
													序号
												</td>
													<td width="15%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													供应商
												</td>
												
												<td width="15%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													品牌名称
												</td>
												<td width="5%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													机型
												</td>
													<td width="15%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													数量
												</td>
													<td width="15%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													单台返利
												</td>
												<td width="15%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													返利金额
												</td>
													
											</tr>
											
											<c:forEach var="item" items="${result}">
													<tr>
												<td width="5%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.CommonCashBackId }
												</td>
											
													<td width="5%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.SupplierName }
												</td>
												<td width="8%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
														${item.PhoneTypeName }
												</td>
												<td width="8%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.PhoneModel }
												</td>
												<td width="5%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.Amount }
												</td>
												<td width="5%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.UnitPrice }
												</td>
												<td width="5%" height="10" align="right" bgcolor="#f2f2f2"
													class="left_txt2">
													${item.CashBackNum }
												</td>
											</tr>
											</c:forEach>

											<tr>
												<td height="17" colspan="4" align="right">
													&nbsp;
												</td>
											</tr>
										
							<tr style="text-align:right;" align="right">
												<td height="30" colspan="18" align="right" style="font-family: Arial, Helvetica, sans-serif;
						font-size: 12px;
						line-height: 25px;
						color: #000000;
						text-align:right;">
													
												共有<strong>${pageUtil.allRecord }</strong> 条记录，当前第<strong> ${pageUtil.currentPage }</strong> 页，共 <strong>${pageUtil.allPage }</strong> 页
												<img src="images/main_54.gif" width="40" height="15" onclick="show(1)" style="cursor: pointer;"/>
          <img src="images/main_56.gif" width="45" height="15" onclick="show(${pageUtil.currentPage-1})" style="cursor: pointer;"/>
           <img src="images/main_58.gif" width="45" height="15" onclick="show(${pageUtil.currentPage+1})" style="cursor: pointer;"/>
           <img src="images/main_60.gif" width="40" height="15" onclick="show(${pageUtil.allPage})" style="cursor: pointer;"/>
           转到
           
              <input type="text" name="page" id="textfield"  style="width:20px; height:18px; font-size:12px; border:solid 1px #7aaebd;"/>
            
           页
           <img src="images/main_62.gif" width="26" height="15"  style="cursor: pointer;"/>
<!--            <img src="images/main_62.gif" width="26" height="15" onclick="show(page.value)" style="cursor: pointer;"/> -->
												
												</td>
											</tr>
	</table>
  </body>
</html>
