
function confirmSubmit(alertMsg,theFrm,actionUrl)
{
if(confirm(alertMsg))
{
if(""!=actionUrl)
{
  theFrm.action=actionUrl;
}
theFrm.submit();
}
} 
function submitFrm(theFrm,actionUrl)
{
if(""!=actionUrl)
{
  theFrm.action=actionUrl;
}
theFrm.submit();
}
 
function confirmClick(alertMsg,url)
{
if(confirm(alertMsg))
{
window.location=url;
}
}

function redirect(url)
{
window.location=url;
}
function historyBack()
{
if(history.lengh>0)
{
 history.back();
}else
{
 window.close();
}
}
//add element to select list <SELECT>
function addOption(selectId,val,label){
	var parentObj = document.getElementById(selectId);
	if(parentObj.type != "select-multiple"){
		return;
	}
	for(var j = 0; j < parentObj.options.length; j++){
	if(parentObj.options[j].id==val)
	{
	return;
	}
	}
	var col_opt = document.createElement("option");
	col_opt.id =val;
	col_opt.name = val;
	col_opt.value = val;
	col_opt.text = label;
	parentObj.options.add(col_opt);
}
//select all options of multi select 
function selectAllOptions(selectId){
	var selectObj = document.getElementById(selectId);
	if(selectObj.type != "select-multiple"){
		return;
	}
	for(var i = 0;i<selectObj.options.length;i++){
		selectObj.options[i].selected = true;
	}
}
//remove child from element
function removeOptions(selectId,childId){
	var selectObj = document.getElementById(selectId);
	if(selectObj.type != "select-multiple"){
		return;
	}
	if(selectObj != null){
	for(var i = 0;i<selectObj.options.length;i++){
	  if(selectObj.options[i].id==childId)
	  {
	  selectObj.removeChild(selectObj.options[i]);
	  return;
	  }
	  }
	
	}
}
//move up/down options in SELECT element
function moveOptions(selectId,n){
	var parentObj = document.getElementById(selectId);
	if(parentObj.type != "select-multiple"){
		return;
	}	
	if(n == 0){
		return;
	}
	if(n+parentObj.selectedIndex>parentObj.length-1||n+parentObj.selectedIndex<0){
	   return;
	   }
	var selectedIdx = new Array();
	for(var j = 0; j < parentObj.options.length; j++){
		if(parentObj.options[j].selected){
			selectedIdx[selectedIdx.length] = j;
		}
		parentObj.options[j].selected = false;
	}
	if(n < 0){
		for(var i = 0; i < selectedIdx.length; i++){
			if(selectedIdx[i] == 0){
				continue;
			}
			swapOptions(selectId,selectedIdx[i], n);
		}			
	} else {
		for(var i = selectedIdx.length - 1; i >= 0 ; i--){
			if(selectedIdx[i] == parentObj.options.length - 1){
				continue;
			}
			swapOptions(selectId,selectedIdx[i], n);
		}
	}
	for(var k = 0; k < selectedIdx.length; k++){
		parentObj.options[selectedIdx[k] + n].selected = true;
	}
}
//swap opotions in select element
function swapOptions(selectId,idx, n){
	var parentObj = document.getElementById(selectId);
	if(parentObj.type != "select-multiple"){
		return;
	}
	
	var col_moved_id = parentObj.options[idx].id;
	var col_moved_name = parentObj.options[idx].name;
	var col_moved_value = parentObj.options[idx].value;
	var col_moved_text = parentObj.options[idx].text;
	
	parentObj.options[idx].id = parentObj.options[idx + n].id;
	parentObj.options[idx].name = parentObj.options[idx + n].name;
	parentObj.options[idx].value = parentObj.options[idx + n].value;
	parentObj.options[idx].text = parentObj.options[idx + n].text;
	
	parentObj.options[idx + n].id = col_moved_id;
	parentObj.options[idx + n].name = col_moved_name;
	parentObj.options[idx + n].value = col_moved_value;
	parentObj.options[idx + n].text = col_moved_text;
}
//clear options of select element 
function clearOptions(selectId){
	var parentObj = document.getElementById(selectId);
	if(parentObj.type != "select-multiple"){
		return;
	}
	for(var i = 0; i < parentObj.options.length; i++){
		parentObj.removeChild(parentObj.options[i]);
	}
}		


// Wrapper class for 'checkBoxSelectAll' global variables
function checkBox_Globals() {
  // Remembers whether we toggled checkboxes on/off when the "all" button was
  // last clicked.
  this.doToggleAll = false;
}

var checkBox_Globals = new checkBox_Globals();

// Toggles selection of supplied checkboxes on and off alternately.
function checkBoxSelectAll(checkboxID) {
 var checkBoxes=document.getElementsByName(checkboxID);
  if( checkBoxes== null)
  return;
  checkBoxes=toArray(checkBoxes);
   checkBox_Globals.doToggleAll = !checkBox_Globals.doToggleAll;
 
  for(  i=0; i<checkBoxes.length; i++ ) {
   if (checkBoxes[i].checked != checkBox_Globals.doToggleAll) {
      // Simulate clicking so events can be triggered
      checkBoxes[i].click();
    }
  }
}

function selectedCheckboxCount(form){
 var length =0;
 var i=0;
 var count =0;
 eles = form.elements;
 while(i<eles.length){
  obj= eles.item(i);
  type = obj.attributes.item("type").nodeValue;
  if(type == "checkbox"){
   if(obj.checked){
    count++;
   }
  }
  i++;
 }
 return count;
}
// Returns an wrapper array if the argument is a single value.
// Returns the argument straight away if it is already an array.
function toArray(value) {
  if (value != null && value[0] != null) 
  {
    return value;
  } 
  return new Array(value);
}

var childwin = null;
function ShowWindow(url,w,h,sb){
  	var wh = document.body.clientHeight;
	var ww = document.body.clientWidth;
  	var wl = (ww - w) / 3;
  	var wt = (wh - h) / 3;
	childwin = window.open(url,'childwin','toolbar=no, menubar=no,location=no, status=no,scrollbars=' + sb + ',resizable=no,top=' + wt + ',left=' + wl + ', width='+ w +',height=' + h);
}

function CheckChildwin(){
  	if(childwin != null){
		if(!childwin.closed){
			childwin.focus();
		} else {
			childwin = null;
		}
  	}
}

function showmodaldiv(title, width, height, src, dac){	
	var d;
	d = window.top.document;
	var div_overlay = d.createElement("div");
 	div_overlay.style.cssText = "text-align: center; width: 100%; height: " + d.body.scrollHeight + "px; position: absolute; top: 0px; left: 0px; z-index:500;";
	div_overlay.id="moduleDiv";	
	var div_groundLayer = d.createElement("div");
//	div_groundLayer.style.cssText="position: absolute; left: 0px; top: 0px; width: 100%; height: " + d.body.scrollHeight + "px; z-index: 999; background-color: black; opacity: 0.5; filter: alpha(opacity =50); color: yellow; border: 1px solid yellow; text-align: center";	
	var div_modal = d.createElement("div");
   	if(isFirefox=navigator.userAgent.indexOf("Firefox")>0){
        div_groundLayer.style.cssText="position: absolute; left: 0px; top: 0px; width: 100%; height: " + d.body.scrollHeight + "px; z-index: 999; background-color: black; opacity: 0.5; color: yellow; border: 1px solid yellow; text-align: center";
   	}else{
   		div_groundLayer.style.cssText="position: absolute; left: 0px; top: 0px; width: 100%; height: " + d.body.scrollHeight + "px; z-index: 999; background-color: black; filter: alpha(opacity =50); color: yellow; border: 1px solid yellow; text-align: center";
   	}
	var div_modal_width = width;
	var div_modal_height = height;
	var div_modal_top = d.body.scrollTop + 100;
	if(isFirefox=navigator.userAgent.indexOf("Firefox")>0){
        div_modal.style.cssText = "position: relative; top: " + div_modal_top + "px; margin: auto; background-color: #fff; width: " + div_modal_width + "px; height: " + div_modal_height + "px; z-index:2000; background-color:#fff; opacity:1; border: 2px solid #FFA500;"
   	} else{
   		div_modal.style.cssText = "position: relative; top: " + div_modal_top + "px; margin: auto; background-color: #fff; width: " + div_modal_width + "px; height: " + div_modal_height + "px; z-index:2000; background-color:#fff; filter: alpha(opacity =100);  border: 2px solid #FFA500;"
   	}
	var div_modal_title_height = 25;
	var div_modal_title = d.createElement("div");
	div_modal_title.style.cssText = "width: 100%; height: " + div_modal_title_height + "px; background-color: #FFA500;border-top: none; text-align: left;";
	var div_title = d.createElement("div");
	div_title.style.cssText = "height: " + div_modal_title_height + "px; line-height: " + div_modal_title_height + "px; margin-left: 10px; font-weight: bold;";
	div_title.innerHTML = title;
	
	var div_close = d.createElement("div");
	div_close.style.cssText = "height: " + div_modal_title_height + "px; line-height: " + div_modal_title_height + "px; position: absolute; top: 0px; right: 0px; margin-right: 10px; cursor: pointer;";
	div_close.innerHTML = "<img style=\" \" src=\"images/close.gif\" title=\"close\"/>";
	div_close.onclick = function(){
	    if(typeof dac != "function"){
	        alert("parameter dac is not a function!");
	    } else {
	        dac(div_modal_iframe);
	    }
		div_overlay.style.display = "none";
		div_overlay = null;		
	}
	
	div_modal_title.appendChild(div_title);
	div_modal_title.appendChild(div_close);
	var adjHeight = 2;
	div_modal_iframe_height = div_modal_height - div_modal_title_height - adjHeight;
	var div_modal_iframe = d.createElement("iframe");
	div_modal_iframe.style.cssText = "width: 100%; height: " + div_modal_iframe_height + "px;border: none; overflow-x: hidden;";
	div_modal_iframe.src = src;
	div_modal_iframe.scrolling = "auto";
	
	div_modal.appendChild(div_modal_title);
	div_modal.appendChild(div_modal_iframe);
	div_overlay.appendChild(div_groundLayer);
	div_overlay.appendChild(div_modal);
	
	d.body.appendChild(div_overlay);
}