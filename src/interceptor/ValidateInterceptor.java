package interceptor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import services.LogServices;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import entity.LogInfo;
import entity.UserInfo;

public class ValidateInterceptor extends AbstractInterceptor {

	// 登录拦截
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		String result = "";
		// System.out.println("开始进行校检拦截");
		String method = invocation.getProxy().getMethod();
		// System.out.println("正在执行："+method);
		if (method.equals("login") || method.equals("regist")
				|| method.equals("userNameExist")) {
			result = invocation.invoke();
		} else {
			Map<String, Object> map = invocation.getInvocationContext()
					.getSession();
			UserInfo user = (UserInfo) map.get("user");
			if (user == null) {
				System.out.println("未登录！");
				result = "noLogin";
			} else {
				// 往logInfo添加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String now = sdf.format(new Date());
				BeanFactory factory = WebApplicationContextUtils
						.getRequiredWebApplicationContext(ServletActionContext
								.getServletContext());
				LogServices logServices = (LogServices) factory
						.getBean("LogServices");
				// LogServices logServices=new LogServices();
				LogInfo logInfo = new LogInfo();
				logInfo.setLogOptOwner(user.getUserInfoName());
				logInfo.setLogFunctionName(method);
				logInfo.setLogOptTime(now);
				logServices.addlog(logInfo);
				result = invocation.invoke();

			}
		}
		// System.out.println("校检拦截结束了");
		return result;
	}
}
