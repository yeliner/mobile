package job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SimpleTrigger;
import org.springframework.scheduling.quartz.QuartzJobBean;

import entity.LogInfo;

import services.LogServices;

public class LogJob extends QuartzJobBean {
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private LogServices logServices;

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		// 实例化
		logServices = (LogServices) arg0.getJobDetail().getJobDataMap()
				.get("logServices");
		// 执行业务方法

		quartzQuery();

	}

	public void quartzQuery() {
		String now = sdf.format(new Date());
		System.out.println("\t\t***********************************");
		System.out.println("\t\t在" + now + "执行了查询任务\t\t\t");
		List<LogInfo> list = logServices.getAlllogs();
		if (list.size() > 0) {
			System.out.println("\t\t操作者\t操作方法\t操作时间");
			for (LogInfo logInfo : list) {
				System.out.println("\t\t" + logInfo.getLogOptOwner().trim()
						+ "\t" + logInfo.getLogFunctionName().trim() + "\t"
						+ logInfo.getLogOptTime());
			}
		}
		System.out.println("\t\t***********************************");
	}

}
