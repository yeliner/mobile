package action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import entity.UserInfo;

import services.UserServices;

public class JsonAction {
	List<UserInfo> list = new ArrayList<UserInfo>();
	private UserServices us;

	public UserServices getUs() {
		return us;
	}

	public void setUs(UserServices us) {
		this.us = us;
	}

	public void getUserByName() throws IOException {
		HttpServletRequest request = ServletActionContext.getRequest();
		String userName = request.getParameter("userName");
		UserInfo user = us.findUser(userName);
		list.add(user);
		outJson();

	}

	public void getAll() throws IOException {
		list = us.getAllUsers();
		outJson();

	}

	private void outJson() throws IOException {
		HttpServletResponse rep = ServletActionContext.getResponse();
		rep.setContentType("text/json;charset=utf-8");
		PrintWriter pw = rep.getWriter();
		JSONArray array = JSONArray.fromObject(list);
		//System.out.println(array);
		pw.print(array);
		pw.flush();
		pw.close();
	}

	public void getSessionUser() throws IOException {
		list.clear();
		UserInfo user = ((UserInfo) ActionContext.getContext().getSession()
				.get("user"));
		if (!list.contains(user)) {
			list.add(user);
		}
	
		outJson();
	}
}
