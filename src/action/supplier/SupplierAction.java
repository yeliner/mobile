package action.supplier;

import java.util.List;

import action.BaseAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

import entity.Supplier;
import entity.SupplierSort;
import services.SupplierServices;
import services.SupplierSortServices;

public class SupplierAction extends BaseAction implements ModelDriven<Supplier> {
	private Supplier supplier = new Supplier();

	private SupplierSortServices sss;
	 
	private SupplierServices sServices;
	
	public void setSss(SupplierSortServices sss) {
		this.sss = sss;
	}
	
	
	public void setsServices(SupplierServices sServices) {
		this.sServices = sServices;
	}


	public Supplier getModel() {
		// TODO Auto-generated method stub
		return supplier;
	}

	
	
	
	
	
	private int updateSupplierId;


	public int getUpdateSupplierId() {
		return updateSupplierId;
	}

	public void setUpdateSupplierId(int updateSupplierId) {
		this.updateSupplierId = updateSupplierId;
	}


	public String goUpdate() {
	
		List<SupplierSort> list = sss.getAllSupplierSort();
		ActionContext.getContext().getSession().put("list", list);
		Supplier supplierUpdate = sServices.queryById(updateSupplierId);
		ActionContext.getContext().getSession().put("supplier", supplierUpdate);
		return "toUpdate";
	}

	
	
	private int supplierSortId;

	public int getSupplierSortId() {
		return supplierSortId;
	}

	public void setSupplierSortId(int supplierSortId) {
	
		this.supplierSortId = supplierSortId;
	}
	
	public String update() {
		supplier.setSupplierSort(sss.queryById(supplierSortId));
		if (sServices.updateSupplier(supplier)) {//this.out("修改成功！");
			System.out.println("Supplier修改成功！");
			return "updateSuccess";
		} else {
			this.out("Supplier update fail");
			return "updateFail";
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private int  deleteSupplierId;
	
	public int getDeleteSupplierId() {
		return deleteSupplierId;
	}

	public void setDeleteSupplierId(int deleteSupplierId) {
		this.deleteSupplierId = deleteSupplierId;
	}

	public String delete(){
		if(sServices.deleteSupplier(deleteSupplierId)){
			return "deleteSuccess";
		}else {
		return	"deleteFail";
		}
		
	}
}
