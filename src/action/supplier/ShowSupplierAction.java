package action.supplier;


import java.util.List;

import util.PageUtil;

import action.BaseAction;

import dao.*;


import entity.*;

public class ShowSupplierAction extends BaseAction {
	private	 SupplierDao sd;
	
	public void setSd(SupplierDao sd) {
		this.sd = sd;
	}

	private String page;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String showSupplier() {

		PageUtil pageUtil = new PageUtil();

		pageUtil.setAllRecord(sd.countAllNum());

		pageUtil.count(this.getPage());
		List<Supplier> list = sd.queryByPage(pageUtil.getPageSize(),
				pageUtil.getCurrentPage());
		pageUtil.setCurrentList(list);
		this.getRequest().setAttribute("pageUtil", pageUtil);

		return "success";
	}

}
