package action.supplier;

import services.SupplierServices;
import services.SupplierSortServices;

import action.BaseAction;

import com.opensymphony.xwork2.ModelDriven;

import entity.Supplier;
import entity.SupplierSort;

public class AddSupplierAction implements
		ModelDriven<Supplier> {
	private SupplierSortServices sss;
	 
	private SupplierServices sServices;
	
	public void setSss(SupplierSortServices sss) {
		this.sss = sss;
	}

	public void setsServices(SupplierServices sServices) {
		this.sServices = sServices;
	}

	private Supplier s = new Supplier();

	public Supplier getModel() {
		// TODO Auto-generated method stub
		return s;
	}


	private int supplierSortId;

	public int getSupplierSortId() {
		return supplierSortId;
	}

	public void setSupplierSortId(int supplierSortId) {
		this.supplierSortId = supplierSortId;
	}

	public String addSupplier() {
		s.setSupplierSort(sss.queryById(supplierSortId));
		if (sServices.addSupplier(s)) {
			System.out.println("Supplier����ʧ��");
			return "success";
		} else {
			System.out.println("Supplier����ʧ�ܣ�");
			return "fail";
		}
	}

}
