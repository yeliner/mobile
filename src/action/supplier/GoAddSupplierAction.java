package action.supplier;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;

import action.BaseAction;

import entity.SupplierSort;

import services.SupplierSortServices;

public class GoAddSupplierAction{
	private SupplierSortServices ssServices;

	public void setSsServices(SupplierSortServices ssServices) {
		this.ssServices = ssServices;
	}

	public String goAddSupplier() {
		List<SupplierSort> list = ssServices.getAllSupplierSort();
		ActionContext.getContext().getSession().put("list", list);
		return "AddSupplier";
	}
}
