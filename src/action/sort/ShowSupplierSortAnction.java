package action.sort;
import java.util.List;

import util.PageUtil;

import action.BaseAction;


import dao.SupplierSortDao;

import entity.SupplierSort;

public class ShowSupplierSortAnction extends BaseAction {
	private SupplierSortDao ssd;
	 
	
	public void setSsd(SupplierSortDao ssd) {
		this.ssd = ssd;
	}

	private String page;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String showSort() {

		PageUtil pageUtil = new PageUtil();
		

		pageUtil.setAllRecord(ssd.countAllNum());

		pageUtil.count(this.getPage());
		List<SupplierSort> list = ssd.queryByPage(pageUtil.getPageSize(),
				pageUtil.getCurrentPage());
		pageUtil.setCurrentList(list);
		this.getRequest().setAttribute("pageUtil", pageUtil);

		return "show";
	}

}
