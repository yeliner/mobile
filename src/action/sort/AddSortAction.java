package action.sort;


import services.SupplierSortServices;

import com.opensymphony.xwork2.ModelDriven;

import entity.SupplierSort;

import action.BaseAction;
public class AddSortAction extends BaseAction implements
		ModelDriven<SupplierSort> {
	private SupplierSortServices ssServices;

	public void setSsServices(SupplierSortServices ssServices) {
		this.ssServices = ssServices;
	}



	private SupplierSort s = new SupplierSort();

	public SupplierSort getModel() {
		// TODO Auto-generated method stub
		return s;
	}



	public String addSort() {
		if (ssServices.addSupplierSort(s)) {
			//this.out("添加成功！");
			System.out.println("SupplierSort添加成功！");
			return "success";
		} else {
			this.out("SupplierSort添加失败！");
			return "fail";
		}
	}
}
