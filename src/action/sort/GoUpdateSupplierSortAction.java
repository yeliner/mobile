package action.sort;


import com.opensymphony.xwork2.ActionContext;

import action.BaseAction;
import entity.SupplierSort;
import services.SupplierSortServices;

public class GoUpdateSupplierSortAction extends BaseAction {
	private SupplierSortServices ssServices;
	 
	public void setSsServices(SupplierSortServices ssServices) {
		this.ssServices = ssServices;
	}
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String goUpdateSort() {
		SupplierSort supplierSort = ssServices.queryById(id);
		ActionContext.getContext().getSession().put("supplierSort", supplierSort);
		return "toUpdateSupplierSort";
	}
}
