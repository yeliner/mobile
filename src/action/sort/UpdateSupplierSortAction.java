package action.sort;

import services.SupplierSortServices;

import action.BaseAction;

import com.opensymphony.xwork2.ModelDriven;

import entity.SupplierSort;

public class UpdateSupplierSortAction extends BaseAction implements
		ModelDriven<SupplierSort> {
	private SupplierSortServices ssServices;
	 
	public void setSsServices(SupplierSortServices ssServices) {
		this.ssServices = ssServices;
	}
	private SupplierSort s = new SupplierSort();

	public SupplierSort getModel() {
		// TODO Auto-generated method stub
		return s;
	}

	
	public String updateSort() {
		if (ssServices.updateSupplierSort(s)) {
			//this.out("修改成功！");
			System.out.println("SupplierSort修改成功");
			return "success";
		} else {
			 this.out("SupplierSort修改失败！");
			return "fail";
		}
	}
}