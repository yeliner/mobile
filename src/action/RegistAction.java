package action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ModelDriven;

import services.UserServices;

import entity.UserInfo;

public class RegistAction implements ModelDriven<UserInfo> {
	private UserInfo userInfo = new UserInfo();
	private UserServices us;

	public UserServices getUs() {
		return us;
	}

	public void setUs(UserServices us) {
		this.us = us;
	}
	public UserInfo getModel() {
		// TODO Auto-generated method stub
		return userInfo;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String regist() {
		if (us.addUser(userInfo)) {
			System.out.println("注册成功！新增用户为："+userInfo.getUserInfoName());
			return "success";

		}
		return "fail";
	}

	

	public void userNameExist() {
		HttpServletRequest request = ServletActionContext.getRequest();
		String userInfoName = request.getParameter("userInfoName");

		UserInfo user = us.findUser(userInfoName);
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out;
		try {
			out = response.getWriter();
			if (user != null) {
				out.print("该用户已经注册");
			} else {
				out.print("该用户名通过");
			}
			out.flush();
			out.close();
		} catch (IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	
}
