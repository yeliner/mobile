package action;

import java.util.List;

import services.*;
import util.PageUtil;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import dao.*;

import entity.*;

public class OrganizationAnction extends BaseAction {
	private OrganizationDao std ;
	
	public void setStd(OrganizationDao std) {
		this.std = std;
	}

	private String page;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String showOrganization() {
		PageUtil pageUtil = new PageUtil();
	

		pageUtil.setAllRecord(std.countAllNum());

		pageUtil.count(this.getPage());
		List<Organization> list = std.queryByPage(pageUtil.getPageSize(),
				pageUtil.getCurrentPage());
		pageUtil.setCurrentList(list);
		this.getRequest().setAttribute("pageUtil", pageUtil);
		return "success";

	}
}