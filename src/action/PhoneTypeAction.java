package action;


import java.util.List;

import services.*;
import util.PageUtil;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import dao.*;

import entity.*;

public class PhoneTypeAction extends BaseAction implements ModelDriven<PhoneType> {
	private PhoneType phoneType = new PhoneType();

	private PhoneTypeServices ptServices;
	private PhoneTypeDao std ;

	public void setPtServices(PhoneTypeServices ptServices) {
		this.ptServices = ptServices;
	}

	public void setStd(PhoneTypeDao std) {
		this.std = std;
	}

	public PhoneType getModel() {
		// TODO Auto-generated method stub
		return phoneType;
	}
	
	
	private int updatePhoneTypeId;

	public int getUpdatePhoneTypeId() {
		return updatePhoneTypeId;
	}

	public void setUpdatePhoneTypeId(int updatePhoneTypeId) {
		this.updatePhoneTypeId = updatePhoneTypeId;
	}
	
	
	public int deletePhoneTypeId;

	public int getDeletePhoneTypeId() {
		return deletePhoneTypeId;
	}

	public void setDeletePhoneTypeId(int deletePhoneTypeId) {
		this.deletePhoneTypeId = deletePhoneTypeId;
	}
	
	
	
	private String page;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}
	
	
	
public String showPhoneType() {
	PageUtil pageUtil = new PageUtil();

	pageUtil.setAllRecord(std.countAllNum());

	pageUtil.count(this.getPage());
	List<PhoneType> list = std.queryByPage(pageUtil.getPageSize(),
			pageUtil.getCurrentPage());
	pageUtil.setCurrentList(list);
	this.getRequest().setAttribute("pageUtil", pageUtil);
return "showSuccess";

}
public String goUpdatePhoneType() {

	PhoneType phoneTypeUpdate = ptServices.queryById(updatePhoneTypeId);
	ActionContext.getContext().getSession().put("phoneType", phoneTypeUpdate);
	return "toUpdatePhoneType";
}

public String updatePhoneType() {
	if (ptServices.updatePhoneType(phoneType)) {
		System.out.println("phoneType修改成功！");
		//this.out("修改成功！");
		return "updateSuccess";
	} else {
		System.out.println("phoneType修改失敗");
		return "updateFail";
	}
}

public String deletePhoneType(){
	if(ptServices.deletePhoneType(deletePhoneTypeId)){
		System.out.println("phoneType刪除成功！");
		return "deleteSuccess";
	}else {

		System.out.println("phoneType刪除失敗！");
	return	"deleteFail";
	}
	
}
public String goAddPhoneType(){
	return "goAdd";
}
public String addPhoneType(){
	if (ptServices.addPhoneType(phoneType)) {
		System.out.println("phoneType添加成功");
		return "addSuccess";
	}
	System.out.println("phoneType添加失敗");
	return "addFail";
			
}

public String execute(){
	return showPhoneType();
}
}

