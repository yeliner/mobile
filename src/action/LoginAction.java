package action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;

import org.apache.struts2.ServletActionContext;

import services.UserServices;
import sun.org.mozilla.javascript.internal.serialize.ScriptableInputStream;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.sun.xml.internal.ws.transport.http.HttpAdapter;

import dao.UserDao;

import entity.UserInfo;
import entity.UserInfo;

public class LoginAction extends BaseAction implements ModelDriven<UserInfo> {
	private UserServices us;
	
	public UserServices getUs() {
		return us;
	}

	public void setUs(UserServices us) {
		this.us = us;
	}

	private UserInfo userInfo = new UserInfo();

	public UserInfo getModel() {
		// TODO Auto-generated method stub
		return userInfo;
	}

	private String verifycode;

	public String getVerifycode() {
		return verifycode;
	}

	public void setVerifycode(String verifycode) {
		this.verifycode = verifycode;
	}

	public String login() {
		String check = (String)ActionContext.getContext().getSession().get("check");
		if (!check.equals(verifycode)) {
			System.out.println("验证码输入不正确");
			// this.out("验证码输入不正确！");
			return "fail";
		} else {
			
			UserInfo user = us.findUser(userInfo.getUserInfoName(),
					userInfo.getUserInfoPsw());
			if (user != null) {
				ActionContext.getContext().getSession().put("user", user);
				return "success";
			} else {
				System.out.println("密码或用户名输入错误");
				return "fail";
			}
		}

	}

}
