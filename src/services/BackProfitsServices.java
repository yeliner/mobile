package services;

import java.util.List;

import dao.BackProfitsDao;

import entity.*;

public class BackProfitsServices {
	// 查找所有的供应商分类
	private BackProfitsDao bpd;

	public BackProfitsDao getBpd() {
		return bpd;
	}

	public void setBpd(BackProfitsDao bpd) {
		this.bpd = bpd;
	}

	public List<BackProfits> getAllBackProfits() {
		return bpd.getAllBackProfits();
	}
}
