package services;

import java.util.List;

import dao.SupplierDao;

import entity.Supplier;
import entity.SupplierSort;

public class SupplierServices {
	private SupplierDao sd;
	

	public SupplierDao getSd() {
		return sd;
	}
	public void setSd(SupplierDao sd) {
		this.sd = sd;
	}
	public List<Supplier> getAllSupplier() {
		return sd.getAllSupplier();
	}
	public List queryByPage(int pageSize, int currentPage) {
		return sd.queryByPage(pageSize, currentPage);
	}
	public boolean addSupplier(Supplier supplier) {
		return sd.addSupplier(supplier);
	}
	public Supplier queryById(int supplierId) {
		return sd.queryById(supplierId);
	}
	public  boolean deleteSupplier(int supplierId){
		return sd.deleteSupplier(supplierId);
	}
	public boolean updateSupplier(Supplier supplier) {
		return sd.updateSupplier(supplier);
	}
}
