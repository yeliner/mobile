package services;

import java.util.List;

import dao.PhoneColorDao;

import entity.*;

public class PhoneColorServices {
	//查找所有的供应商分类
	private PhoneColorDao pcd;
	
			public PhoneColorDao getPcd() {
		return pcd;
	}

	public void setPcd(PhoneColorDao pcd) {
		this.pcd = pcd;
	}

			public List<PhoneColor> getAllPhoneColor(){
				return pcd.getAllPhoneColor();
			}	
}
