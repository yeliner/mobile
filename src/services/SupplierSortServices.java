package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;



import dao.SupplierSortDao;

import entity.SupplierSort;
public class SupplierSortServices {
	// 查找所有的供应商分类
	private SupplierSortDao ssd;


	public void setSsd(SupplierSortDao ssd) {
		this.ssd = ssd;
	}

	public List<SupplierSort> queryByPage(int pageSize, int currentPage) {

		return ssd.queryByPage(pageSize, currentPage);
	}

	public boolean addSupplierSort(SupplierSort supplierSort) {

		return ssd.addSupplierSort(supplierSort);
	}

	public boolean deleteSupplierSort(int supplierSortId) {

		return ssd.deleteSupplierSort(supplierSortId);
	}
	
	public SupplierSort queryById(int sortId) {
		return ssd.queryById(sortId);
	}
	public boolean updateSupplierSort(SupplierSort supplierSort) {
		return ssd.updateSupplierSort(supplierSort);
	}
	
	 public List<SupplierSort> getAllSupplierSort() {
		 return ssd.getAllSupplierSort();
	 }
}
