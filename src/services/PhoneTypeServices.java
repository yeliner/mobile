package services;

import java.util.List;

import dao.PhoneTypeDao;

import entity.PhoneType;
import entity.SupplierSort;

public class PhoneTypeServices {
	//查找所有的供应商分类
	private PhoneTypeDao ptd;
	
			public PhoneTypeDao getPtd() {
		return ptd;
	}
	public void setPtd(PhoneTypeDao ptd) {
		this.ptd = ptd;
	}
			public List<PhoneType> getAllPhoneType(){
				return ptd.getAllPhoneType();
			}
			public PhoneType queryById(int updatePhoneTypeId) {
				// TODO Auto-generated method stub
				return ptd.queryById(updatePhoneTypeId);
			}
			public boolean updatePhoneType(PhoneType phoneType) {
				// TODO Auto-generated method stub
				return ptd.updatePhoneType(phoneType);
			}
			public boolean deletePhoneType(int deletePhoneTypeId) {
				// TODO Auto-generated method stub
				return ptd.deletePhoneType(deletePhoneTypeId);
			}
			public boolean addPhoneType(PhoneType phoneType) {
				return ptd.addPhoneType(phoneType);
			}
}
