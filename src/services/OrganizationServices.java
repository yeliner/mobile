package services;

import java.util.List;

import dao.OrganizationDao;

import entity.Organization;
import entity.SupplierSort;

public class OrganizationServices {
	//查找所有的供应商分类
	private OrganizationDao pd;
	
			public OrganizationDao getPd() {
		return pd;
	}

	public void setPd(OrganizationDao pd) {
		this.pd = pd;
	}

			public List<Organization> getAllOrganization(){
				
				return pd.getAllOrganization();
			}
}
