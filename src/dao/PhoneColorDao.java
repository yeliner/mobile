package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.base.BaseDAO;

import entity.PhoneColor;



public class PhoneColorDao extends BaseDAO {
	public List queryByPage(int pageSize, int currentPage) {
		String hql = "from PhoneColor as m  order by m.phoneColorId ";
		return this.queryByPage(pageSize, currentPage, hql);
			}
	public List getAllPhoneColor() {
		return this.getObjects();
	}
	public int countAllNum() {
		String sql = "select count(*) from PhoneColor";
		return this.countAllNum(sql);	
	}
	

}
