package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.base.BaseDAO;

import entity.*;

public class PhoneTypeDao extends BaseDAO {
	public List queryByPage(int pageSize, int currentPage) {
		String sql = "from PhoneType as m  order by m.phoneTypeId ";
		return this.queryByPage(pageSize, currentPage, sql);
	}

	public int countAllNum() {
		String sql = "select count(*) from PhoneType";
		return this.countAllNum(sql);
	}

	public List getAllPhoneType() {
		return this.getObjects();

	}

	public PhoneType queryById(int updatePhoneTypeId) {
		return (PhoneType) this.findById(updatePhoneTypeId);

	}

	public boolean deletePhoneType(int deletePhoneTypeId) {
		return this.delete(deletePhoneTypeId);

	}

	public boolean addPhoneType(PhoneType phoneType) {
		phoneType.setPhoneTypeNum(getPhoneTypeNum());
		return this.saveOrUpdate(phoneType);
	}

	public String getPhoneTypeNum() {
		String hql = "select max(phoneTypeId) from PhoneType";
		String num = "PHONE" + this.getNum(hql);
		return num;
	}

	public boolean updatePhoneType(PhoneType phoneType) {
		return this.saveOrUpdate(phoneType);			
	}

}
