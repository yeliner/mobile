package dao.base;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * 公共DAO
 * 
 * @author hr
 * 
 */
public class BaseDAO extends HibernateDaoSupport {
	// 实体对象全路径名称，包括包名
	private String poName = "";

	// 把实体对象增加或者修改同步到数据库
	public boolean saveOrUpdate(BasePO BasePO) {
		try {
		getHibernateTemplate().saveOrUpdate(BasePO);
		return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	// 根据实体对象删除数据记录
	public boolean delete(BasePO po) {
		try {
		this.getHibernateTemplate().delete(po);
		return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	// 根据主键ID删除数据记录
	public boolean delete(Integer id) {
		try {
		BasePO po = findById(id);
		this.delete(po);
		return true;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}
	}

	// 根据主键ID值查找实体对象
	public BasePO findById(Integer id) {
		if (id == null) {
			return null;
		}
		BasePO po = null;
		try {
			po = (BasePO) this.getHibernateTemplate().get(poName, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return po;
	}

	// 查询对象的所有记录
	public List getObjects() {
		List poList = null;
		try {
			String hql = "from " + poName;
			// System.out.println("hql:"+hql);
			poList = this.getHibernateTemplate().find(hql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return poList;
	}

	// HQL，多条件查询
	public List searchByHQL(String hql, Object[] params) {
		Query query = this.getSession().createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i, params[i]);
		}
		return query.list();
	}

	// HQL，单个条件查询
	public List searchByHQL(String hql, Object param) {
		return searchByHQL(hql, new Object[] { param });
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	public List queryByPage(int pageSize, int currentPage, String sql) {
		try {
			Query query = this.getSession().createQuery(sql);
			query.setFirstResult(pageSize * (currentPage - 1));
			query.setMaxResults(pageSize);
			return query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public int countAllNum(String sql) {
		try {
			Query query = this.getSession().createQuery(sql);
			return ((Number) query.uniqueResult()).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String getNum(String hql) {
		String Num = null;
		try {
			Query query = this.getSession().createQuery(hql);
			int no = ((Number) query.uniqueResult()).intValue()+1;
			Num = MessageFormat.format("{0,number,0000}", no);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Num;
	}
}
