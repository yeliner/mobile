package dao;

import java.util.List;
import dao.base.BaseDAO;


public class BackProfitsDao extends BaseDAO{
	public List queryByPage(int pageSize, int currentPage) {
		String sql = "from BackProfits as m  order by m.backProfitsId ";
		return this.queryByPage(pageSize, currentPage, sql);
			}
	public List getAllBackProfits() {
		return this.getObjects();
	}
	public int countAllNum() {
		String sql = "select count(*) from BackProfits";
		return this.countAllNum(sql);		
	}

}
