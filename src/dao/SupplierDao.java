package dao;

import java.util.List;


import dao.base.BaseDAO;


import entity.Supplier;

public class SupplierDao extends BaseDAO{


	public Supplier queryById(int supplierId) {
		return (Supplier) this.findById(supplierId);
		}

	public List queryByPage(int pageSize, int currentPage) {
		String hql = "from Supplier as m  order by m.supplierId ";
		return this.queryByPage(pageSize, currentPage, hql);
	}

	public int countAllNum() {
		String hql = "select count(*) from Supplier";
		return this.countAllNum(hql);

	}

	public List getAllSupplier() {
		return this.getObjects();
	}

	public String getNum() {
		String hql = "select max(supplierId) from Supplier";
		String num = "SUPPLIER" + this.getNum(hql);
		return num;
	}

	public boolean addSupplier(Supplier supplier) {
		
			supplier.setSupplierNum(getNum());
			return this.saveOrUpdate(supplier);
		
	}

	public boolean deleteSupplier(int supplierId) {
		return this.delete(supplierId);
	}

	public boolean updateSupplier(Supplier supplier) {
		return this.saveOrUpdate(supplier);
			
	}

}
