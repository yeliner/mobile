package dao;

import java.util.List;


import dao.base.BaseDAO;

import entity.UserInfo;

public class UserDao extends BaseDAO {
	List list;

	public UserInfo findUser(String userInfoName, String userInfoPsw) {
		String hql = "from UserInfo where userInfoName=? and userInfoPsw=?";
		Object[] params = { userInfoName, userInfoPsw };
		list = this.searchByHQL(hql, params);
		if (list.size() > 0) {
			return (UserInfo) list.get(0);
		}
		return null;
	}

	public UserInfo findUser(String userInfoName) {
		// 注册时不能重名
		// hql语句
		String hql = "from UserInfo where userInfoName=?";
		Object[] param = { userInfoName };
		list = this.searchByHQL(hql, param);
		if (list.size() > 0) {
			return (UserInfo) list.get(0);
		}
		return null;
	}

	public boolean addUser(UserInfo userInfo) {
		return this.saveOrUpdate(userInfo);

	}

	public List queryAll() {
		return this.getObjects();

	}
}