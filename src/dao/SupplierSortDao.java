package dao;

import java.util.List;


import dao.base.BaseDAO;

import entity.SupplierSort;
public class SupplierSortDao extends BaseDAO {

	public List queryByPage(int pageSize, int currentPage) {
		String hql = "from SupplierSort as m  order by m.supplierSortId ";
		return this.queryByPage(pageSize, currentPage, hql);
	}

	public int countAllNum() {
		String hql = "select count(*) from SupplierSort as s";
		return this.countAllNum(hql);
	}

	public String getSortNum() {
		String hql = "select max(supplierSortId) from SupplierSort";
		String num = "SORT" + this.getNum(hql);
		return num;

	}

	public SupplierSort queryById(int sortId) {
		return (SupplierSort) this.findById(sortId);
	}

	public boolean addSupplierSort(SupplierSort supplierSort) {
		
			supplierSort.setSupplierSortNum(this.getSortNum());
			return this.saveOrUpdate(supplierSort);
		

	}

	public boolean deleteSupplierSort(int SupplierSortId) {
		return this.delete(this.findById(SupplierSortId));
			
	}

	public List getAllSupplierSort() {
		return this.getObjects();
	}

	public boolean updateSupplierSort(SupplierSort supplierSort) {
		return this.saveOrUpdate(supplierSort);
			
	}
}