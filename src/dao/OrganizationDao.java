package dao;

import java.util.List;
import dao.base.BaseDAO;



public class OrganizationDao extends BaseDAO {

	public List queryByPage(int pageSize, int currentPage) {
		String sql = "from Organization as m  order by m.organizationId ";
		return this.queryByPage(pageSize, currentPage, sql);
			}
	public List getAllOrganization() {
		return this.getObjects();
	}
	public int countAllNum() {
		String sql = "select count(*) from Organization";
		return this.countAllNum(sql);	
	}

}
