package entity;

import dao.base.BasePO;

/**
 * CashBack entity. @author MyEclipse Persistence Tools
 */

public class CashBack extends BasePO implements java.io.Serializable {

	// Fields

	private Integer cashBackId;
	private Supplier supplier;
	private String startDate;
	private String finishDate;
	private Double cashAmount;
	private Double ratio;
	private Double cashBackNum;
	private String cashBackRemark;
	private Boolean status;
	private String checkDate;
	private String inputDate;

	// Constructors

	/** default constructor */
	public CashBack() {
	}

	/** minimal constructor */
	public CashBack(Supplier supplier, String startDate, String finishDate,
			Double cashAmount, Double ratio, Double cashBackNum,
			String cashBackRemark) {
		this.supplier = supplier;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.cashAmount = cashAmount;
		this.ratio = ratio;
		this.cashBackNum = cashBackNum;
		this.cashBackRemark = cashBackRemark;
	}

	/** full constructor */
	public CashBack(Supplier supplier, String startDate, String finishDate,
			Double cashAmount, Double ratio, Double cashBackNum,
			String cashBackRemark, Boolean status, String checkDate,
			String inputDate) {
		this.supplier = supplier;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.cashAmount = cashAmount;
		this.ratio = ratio;
		this.cashBackNum = cashBackNum;
		this.cashBackRemark = cashBackRemark;
		this.status = status;
		this.checkDate = checkDate;
		this.inputDate = inputDate;
	}

	// Property accessors

	public Integer getCashBackId() {
		return this.cashBackId;
	}

	public void setCashBackId(Integer cashBackId) {
		this.cashBackId = cashBackId;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public String getStartDate() {
		return this.startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate;
	}

	public Double getCashAmount() {
		return this.cashAmount;
	}

	public void setCashAmount(Double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public Double getRatio() {
		return this.ratio;
	}

	public void setRatio(Double ratio) {
		this.ratio = ratio;
	}

	public Double getCashBackNum() {
		return this.cashBackNum;
	}

	public void setCashBackNum(Double cashBackNum) {
		this.cashBackNum = cashBackNum;
	}

	public String getCashBackRemark() {
		return this.cashBackRemark;
	}

	public void setCashBackRemark(String cashBackRemark) {
		this.cashBackRemark = cashBackRemark;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getCheckDate() {
		return this.checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getInputDate() {
		return this.inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

}