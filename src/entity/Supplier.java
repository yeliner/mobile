package entity;

import java.util.HashSet;
import java.util.Set;

import dao.base.BasePO;

/**
 * Supplier entity. @author MyEclipse Persistence Tools
 */

public class Supplier extends BasePO implements java.io.Serializable {

	// Fields

	private Integer supplierId;
	private SupplierSort supplierSort;
	private String supplierNum;
	private String supplierName;
	private String supplierCharge;
	private String supplierPhone;
	private String supplierFax;
	private String supplierAddress;
	private String supplierRemark;
	private Set payments = new HashSet(0);
	private Set cashBacks = new HashSet(0);
	private Set cycleCashs = new HashSet(0);
	private Set commonCashBacks = new HashSet(0);

	// Constructors

	/** default constructor */
	public Supplier() {
	}

	/** minimal constructor */
	public Supplier(SupplierSort supplierSort, String supplierNum,
			String supplierName, String supplierCharge, String supplierPhone,
			String supplierFax, String supplierAddress) {
		this.supplierSort = supplierSort;
		this.supplierNum = supplierNum;
		this.supplierName = supplierName;
		this.supplierCharge = supplierCharge;
		this.supplierPhone = supplierPhone;
		this.supplierFax = supplierFax;
		this.supplierAddress = supplierAddress;
	}

	/** full constructor */
	public Supplier(SupplierSort supplierSort, String supplierNum,
			String supplierName, String supplierCharge, String supplierPhone,
			String supplierFax, String supplierAddress, String supplierRemark,
			Set payments, Set cashBacks, Set cycleCashs, Set commonCashBacks) {
		this.supplierSort = supplierSort;
		this.supplierNum = supplierNum;
		this.supplierName = supplierName;
		this.supplierCharge = supplierCharge;
		this.supplierPhone = supplierPhone;
		this.supplierFax = supplierFax;
		this.supplierAddress = supplierAddress;
		this.supplierRemark = supplierRemark;
		this.payments = payments;
		this.cashBacks = cashBacks;
		this.cycleCashs = cycleCashs;
		this.commonCashBacks = commonCashBacks;
	}

	// Property accessors

	public Integer getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public SupplierSort getSupplierSort() {
		return this.supplierSort;
	}

	public void setSupplierSort(SupplierSort supplierSort) {
		this.supplierSort = supplierSort;
	}

	public String getSupplierNum() {
		return this.supplierNum;
	}

	public void setSupplierNum(String supplierNum) {
		this.supplierNum = supplierNum;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierCharge() {
		return this.supplierCharge;
	}

	public void setSupplierCharge(String supplierCharge) {
		this.supplierCharge = supplierCharge;
	}

	public String getSupplierPhone() {
		return this.supplierPhone;
	}

	public void setSupplierPhone(String supplierPhone) {
		this.supplierPhone = supplierPhone;
	}

	public String getSupplierFax() {
		return this.supplierFax;
	}

	public void setSupplierFax(String supplierFax) {
		this.supplierFax = supplierFax;
	}

	public String getSupplierAddress() {
		return this.supplierAddress;
	}

	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}

	public String getSupplierRemark() {
		return this.supplierRemark;
	}

	public void setSupplierRemark(String supplierRemark) {
		this.supplierRemark = supplierRemark;
	}

	public Set getPayments() {
		return this.payments;
	}

	public void setPayments(Set payments) {
		this.payments = payments;
	}

	public Set getCashBacks() {
		return this.cashBacks;
	}

	public void setCashBacks(Set cashBacks) {
		this.cashBacks = cashBacks;
	}

	public Set getCycleCashs() {
		return this.cycleCashs;
	}

	public void setCycleCashs(Set cycleCashs) {
		this.cycleCashs = cycleCashs;
	}

	public Set getCommonCashBacks() {
		return this.commonCashBacks;
	}

	public void setCommonCashBacks(Set commonCashBacks) {
		this.commonCashBacks = commonCashBacks;
	}

}