package entity;

import dao.base.BasePO;

/**
 * UserInfo entity. @author MyEclipse Persistence Tools
 */

public class UserInfo extends BasePO implements java.io.Serializable {

	// Fields

	private Integer userInfoId;
	private String userInfoName;
	private String userInfoPsw;

	// Constructors

	/** default constructor */
	public UserInfo() {
	}

	/** minimal constructor */
	public UserInfo(String userInfoName) {
		this.userInfoName = userInfoName;
	}

	/** full constructor */
	public UserInfo(String userInfoName, String userInfoPsw) {
		this.userInfoName = userInfoName;
		this.userInfoPsw = userInfoPsw;
	}

	// Property accessors

	public Integer getUserInfoId() {
		return this.userInfoId;
	}

	public void setUserInfoId(Integer userInfoId) {
		this.userInfoId = userInfoId;
	}

	public String getUserInfoName() {
		return this.userInfoName;
	}

	public void setUserInfoName(String userInfoName) {
		this.userInfoName = userInfoName;
	}

	public String getUserInfoPsw() {
		return this.userInfoPsw;
	}

	public void setUserInfoPsw(String userInfoPsw) {
		this.userInfoPsw = userInfoPsw;
	}

}