package entity;

import dao.base.BasePO;

/**
 * CycleCash entity. @author MyEclipse Persistence Tools
 */

public class CycleCash extends BasePO implements java.io.Serializable {

	// Fields

	private Integer cycleId;
	private Supplier supplier;
	private Double incomeCash;
	private String incomeTime;

	// Constructors

	/** default constructor */
	public CycleCash() {
	}

	/** full constructor */
	public CycleCash(Supplier supplier, Double incomeCash, String incomeTime) {
		this.supplier = supplier;
		this.incomeCash = incomeCash;
		this.incomeTime = incomeTime;
	}

	// Property accessors

	public Integer getCycleId() {
		return this.cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Double getIncomeCash() {
		return this.incomeCash;
	}

	public void setIncomeCash(Double incomeCash) {
		this.incomeCash = incomeCash;
	}

	public String getIncomeTime() {
		return this.incomeTime;
	}

	public void setIncomeTime(String incomeTime) {
		this.incomeTime = incomeTime;
	}

}