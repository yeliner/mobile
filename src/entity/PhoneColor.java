package entity;

import dao.base.BasePO;

/**
 * PhoneColor entity. @author MyEclipse Persistence Tools
 */

public class PhoneColor extends BasePO  implements java.io.Serializable {

	// Fields

	private Integer phoneColorId;
	private String phoneColorName;

	// Constructors

	/** default constructor */
	public PhoneColor() {
	}

	/** full constructor */
	public PhoneColor(String phoneColorName) {
		this.phoneColorName = phoneColorName;
	}

	// Property accessors

	public Integer getPhoneColorId() {
		return this.phoneColorId;
	}

	public void setPhoneColorId(Integer phoneColorId) {
		this.phoneColorId = phoneColorId;
	}

	public String getPhoneColorName() {
		return this.phoneColorName;
	}

	public void setPhoneColorName(String phoneColorName) {
		this.phoneColorName = phoneColorName;
	}

}