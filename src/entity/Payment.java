package entity;

import dao.base.BasePO;

/**
 * Payment entity. @author MyEclipse Persistence Tools
 */

public class Payment extends BasePO implements java.io.Serializable {

	// Fields

	private Integer cycleCashId;
	private Supplier supplier;
	private String beginDate;
	private String endDate;
	private Double cycleCash;
	private Boolean cycleStatus;

	// Constructors

	/** default constructor */
	public Payment() {
	}

	/** full constructor */
	public Payment(Supplier supplier, String beginDate, String endDate,
			Double cycleCash, Boolean cycleStatus) {
		this.supplier = supplier;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.cycleCash = cycleCash;
		this.cycleStatus = cycleStatus;
	}

	// Property accessors

	public Integer getCycleCashId() {
		return this.cycleCashId;
	}

	public void setCycleCashId(Integer cycleCashId) {
		this.cycleCashId = cycleCashId;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public String getBeginDate() {
		return this.beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return this.endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Double getCycleCash() {
		return this.cycleCash;
	}

	public void setCycleCash(Double cycleCash) {
		this.cycleCash = cycleCash;
	}

	public Boolean getCycleStatus() {
		return this.cycleStatus;
	}

	public void setCycleStatus(Boolean cycleStatus) {
		this.cycleStatus = cycleStatus;
	}

}