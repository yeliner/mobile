package entity;

import dao.base.BasePO;

/**
 * CommonCashBack entity. @author MyEclipse Persistence Tools
 */

public class CommonCashBack extends BasePO implements java.io.Serializable {

	// Fields

	private Integer commonCashBackId;
	private Supplier supplier;
	private PhoneType phoneType;
	private Integer amount;
	private Double unitPrice;
	private Double cashBackNum;

	// Constructors

	/** default constructor */
	public CommonCashBack() {
	}

	/** full constructor */
	public CommonCashBack(Supplier supplier, PhoneType phoneType,
			Integer amount, Double unitPrice, Double cashBackNum) {
		this.supplier = supplier;
		this.phoneType = phoneType;
		this.amount = amount;
		this.unitPrice = unitPrice;
		this.cashBackNum = cashBackNum;
	}

	// Property accessors

	public Integer getCommonCashBackId() {
		return this.commonCashBackId;
	}

	public void setCommonCashBackId(Integer commonCashBackId) {
		this.commonCashBackId = commonCashBackId;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public PhoneType getPhoneType() {
		return this.phoneType;
	}

	public void setPhoneType(PhoneType phoneType) {
		this.phoneType = phoneType;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Double getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getCashBackNum() {
		return this.cashBackNum;
	}

	public void setCashBackNum(Double cashBackNum) {
		this.cashBackNum = cashBackNum;
	}

}