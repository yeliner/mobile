package entity;

import dao.base.BasePO;

/**
 * LogInfo entity. @author MyEclipse Persistence Tools
 */

public class LogInfo extends BasePO implements java.io.Serializable {

	// Fields

	private Integer logId;
	private String logOptOwner;
	private String logFunctionName;
	private String logOptTime;

	// Constructors

	/** default constructor */
	public LogInfo() {
	}

	/** full constructor */
	public LogInfo(String logOptOwner, String logFunctionName, String logOptTime) {
		this.logOptOwner = logOptOwner;
		this.logFunctionName = logFunctionName;
		this.logOptTime = logOptTime;
	}

	// Property accessors

	public Integer getLogId() {
		return this.logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public String getLogOptOwner() {
		return this.logOptOwner;
	}

	public void setLogOptOwner(String logOptOwner) {
		this.logOptOwner = logOptOwner;
	}

	public String getLogFunctionName() {
		return this.logFunctionName;
	}

	public void setLogFunctionName(String logFunctionName) {
		this.logFunctionName = logFunctionName;
	}

	public String getLogOptTime() {
		return this.logOptTime;
	}

	public void setLogOptTime(String logOptTime) {
		this.logOptTime = logOptTime;
	}

}