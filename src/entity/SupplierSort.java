package entity;

import java.util.HashSet;
import java.util.Set;

import dao.base.BasePO;

/**
 * SupplierSort entity. @author MyEclipse Persistence Tools
 */

public class SupplierSort extends BasePO implements java.io.Serializable {

	// Fields

	private Integer supplierSortId;
	private String supplierSortNum;
	private String supplierSortName;
	private String supplierSortRemark;
	private Set suppliers = new HashSet(0);

	// Constructors

	/** default constructor */
	public SupplierSort() {
	}

	/** minimal constructor */
	public SupplierSort(String supplierSortNum, String supplierSortName) {
		this.supplierSortNum = supplierSortNum;
		this.supplierSortName = supplierSortName;
	}

	/** full constructor */
	public SupplierSort(String supplierSortNum, String supplierSortName,
			String supplierSortRemark, Set suppliers) {
		this.supplierSortNum = supplierSortNum;
		this.supplierSortName = supplierSortName;
		this.supplierSortRemark = supplierSortRemark;
		this.suppliers = suppliers;
	}

	// Property accessors

	public Integer getSupplierSortId() {
		return this.supplierSortId;
	}

	public void setSupplierSortId(Integer supplierSortId) {
		this.supplierSortId = supplierSortId;
	}

	public String getSupplierSortNum() {
		return this.supplierSortNum;
	}

	public void setSupplierSortNum(String supplierSortNum) {
		this.supplierSortNum = supplierSortNum;
	}

	public String getSupplierSortName() {
		return this.supplierSortName;
	}

	public void setSupplierSortName(String supplierSortName) {
		this.supplierSortName = supplierSortName;
	}

	public String getSupplierSortRemark() {
		return this.supplierSortRemark;
	}

	public void setSupplierSortRemark(String supplierSortRemark) {
		this.supplierSortRemark = supplierSortRemark;
	}

	public Set getSuppliers() {
		return this.suppliers;
	}

	public void setSuppliers(Set suppliers) {
		this.suppliers = suppliers;
	}

}