package entity;

import dao.base.BasePO;

/**
 * Organization entity. @author MyEclipse Persistence Tools
 */

public class Organization extends BasePO implements java.io.Serializable {

	// Fields

	private Integer organizationId;
	private String organizationNum;
	private String organizationName;
	private String organizationPhone;
	private String organizationAddress;
	private String organizationCharge;
	private String organizationRemark;

	// Constructors

	/** default constructor */
	public Organization() {
	}

	/** full constructor */
	public Organization(String organizationNum, String organizationName,
			String organizationPhone, String organizationAddress,
			String organizationCharge, String organizationRemark) {
		this.organizationNum = organizationNum;
		this.organizationName = organizationName;
		this.organizationPhone = organizationPhone;
		this.organizationAddress = organizationAddress;
		this.organizationCharge = organizationCharge;
		this.organizationRemark = organizationRemark;
	}

	// Property accessors

	public Integer getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationNum() {
		return this.organizationNum;
	}

	public void setOrganizationNum(String organizationNum) {
		this.organizationNum = organizationNum;
	}

	public String getOrganizationName() {
		return this.organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOrganizationPhone() {
		return this.organizationPhone;
	}

	public void setOrganizationPhone(String organizationPhone) {
		this.organizationPhone = organizationPhone;
	}

	public String getOrganizationAddress() {
		return this.organizationAddress;
	}

	public void setOrganizationAddress(String organizationAddress) {
		this.organizationAddress = organizationAddress;
	}

	public String getOrganizationCharge() {
		return this.organizationCharge;
	}

	public void setOrganizationCharge(String organizationCharge) {
		this.organizationCharge = organizationCharge;
	}

	public String getOrganizationRemark() {
		return this.organizationRemark;
	}

	public void setOrganizationRemark(String organizationRemark) {
		this.organizationRemark = organizationRemark;
	}

}