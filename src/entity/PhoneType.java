package entity;

import java.util.HashSet;
import java.util.Set;

import dao.base.BasePO;

/**
 * PhoneType entity. @author MyEclipse Persistence Tools
 */

public class PhoneType extends BasePO implements java.io.Serializable {

	// Fields

	private Integer phoneTypeId;
	private String phoneTypeNum;
	private String phoneTypeName;
	private String phoneModel;
	private Double takeOutPrice;
	private Double basicBack;
	private Double amountBack;
	private String phoneTypeRemark;
	private Set commonCashBacks = new HashSet(0);

	// Constructors

	/** default constructor */
	public PhoneType() {
	}

	/** minimal constructor */
	public PhoneType(String phoneTypeNum, String phoneTypeName,
			String phoneModel, Double takeOutPrice, Double basicBack,
			Double amountBack) {
		this.phoneTypeNum = phoneTypeNum;
		this.phoneTypeName = phoneTypeName;
		this.phoneModel = phoneModel;
		this.takeOutPrice = takeOutPrice;
		this.basicBack = basicBack;
		this.amountBack = amountBack;
	}

	/** full constructor */
	public PhoneType(String phoneTypeNum, String phoneTypeName,
			String phoneModel, Double takeOutPrice, Double basicBack,
			Double amountBack, String phoneTypeRemark, Set commonCashBacks) {
		this.phoneTypeNum = phoneTypeNum;
		this.phoneTypeName = phoneTypeName;
		this.phoneModel = phoneModel;
		this.takeOutPrice = takeOutPrice;
		this.basicBack = basicBack;
		this.amountBack = amountBack;
		this.phoneTypeRemark = phoneTypeRemark;
		this.commonCashBacks = commonCashBacks;
	}

	// Property accessors

	public Integer getPhoneTypeId() {
		return this.phoneTypeId;
	}

	public void setPhoneTypeId(Integer phoneTypeId) {
		this.phoneTypeId = phoneTypeId;
	}

	public String getPhoneTypeNum() {
		return this.phoneTypeNum;
	}

	public void setPhoneTypeNum(String phoneTypeNum) {
		this.phoneTypeNum = phoneTypeNum;
	}

	public String getPhoneTypeName() {
		return this.phoneTypeName;
	}

	public void setPhoneTypeName(String phoneTypeName) {
		this.phoneTypeName = phoneTypeName;
	}

	public String getPhoneModel() {
		return this.phoneModel;
	}

	public void setPhoneModel(String phoneModel) {
		this.phoneModel = phoneModel;
	}

	public Double getTakeOutPrice() {
		return this.takeOutPrice;
	}

	public void setTakeOutPrice(Double takeOutPrice) {
		this.takeOutPrice = takeOutPrice;
	}

	public Double getBasicBack() {
		return this.basicBack;
	}

	public void setBasicBack(Double basicBack) {
		this.basicBack = basicBack;
	}

	public Double getAmountBack() {
		return this.amountBack;
	}

	public void setAmountBack(Double amountBack) {
		this.amountBack = amountBack;
	}

	public String getPhoneTypeRemark() {
		return this.phoneTypeRemark;
	}

	public void setPhoneTypeRemark(String phoneTypeRemark) {
		this.phoneTypeRemark = phoneTypeRemark;
	}

	public Set getCommonCashBacks() {
		return this.commonCashBacks;
	}

	public void setCommonCashBacks(Set commonCashBacks) {
		this.commonCashBacks = commonCashBacks;
	}

}