package entity;

import dao.base.BasePO;

/**
 * BackProfits entity. @author MyEclipse Persistence Tools
 */

public class BackProfits  extends BasePO implements java.io.Serializable {

	// Fields

	private Integer backProfitsId;
	private String backProfitsNum;
	private String backProfitsName;
	private String backProfitsRemark;

	// Constructors

	/** default constructor */
	public BackProfits() {
	}

	/** full constructor */
	public BackProfits(String backProfitsNum, String backProfitsName,
			String backProfitsRemark) {
		this.backProfitsNum = backProfitsNum;
		this.backProfitsName = backProfitsName;
		this.backProfitsRemark = backProfitsRemark;
	}

	// Property accessors

	public Integer getBackProfitsId() {
		return this.backProfitsId;
	}

	public void setBackProfitsId(Integer backProfitsId) {
		this.backProfitsId = backProfitsId;
	}

	public String getBackProfitsNum() {
		return this.backProfitsNum;
	}

	public void setBackProfitsNum(String backProfitsNum) {
		this.backProfitsNum = backProfitsNum;
	}

	public String getBackProfitsName() {
		return this.backProfitsName;
	}

	public void setBackProfitsName(String backProfitsName) {
		this.backProfitsName = backProfitsName;
	}

	public String getBackProfitsRemark() {
		return this.backProfitsRemark;
	}

	public void setBackProfitsRemark(String backProfitsRemark) {
		this.backProfitsRemark = backProfitsRemark;
	}

}